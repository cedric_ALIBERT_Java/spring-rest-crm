package fr.cedricalibert.springdemo.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomerRestExceptionHandler {
	// handler for customerNotFoundException
	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleException(CustomerNotFoundException exc){
		
		//create customer error response object
		CustomerErrorResponse customerErrorResponse = new CustomerErrorResponse(
				HttpStatus.NOT_FOUND.value(), 
				exc.getMessage(), 
				System.currentTimeMillis()
				);
		
		return new ResponseEntity<CustomerErrorResponse>(customerErrorResponse, HttpStatus.NOT_FOUND);
	}
	
	// handler for all exception
	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleException(Exception exc){
		
		//create customer error response object
		CustomerErrorResponse customerErrorResponse = new CustomerErrorResponse(
				HttpStatus.BAD_REQUEST.value(), 
				exc.getMessage(), 
				System.currentTimeMillis()
				);
		
		return new ResponseEntity<CustomerErrorResponse>(customerErrorResponse, HttpStatus.BAD_REQUEST);
	}
}
